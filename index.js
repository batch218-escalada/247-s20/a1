// [SECTION] Part 1

let number = parseInt(prompt('Enter a number:'));
console.log('The number you provided is ' + number);

for (let i = number; i >= 0; i--) {
    if (i <= 50) {
        console.log('The current value is at ' + i + '. Terminating the loop.');
        break;
    }

    else if (i % 10 === 0) {
        console.log('The number is divisible by 10. Skipping the number.');
    }

    else if (i % 5 === 0) {
        console.log(i);
    };
};

// [SECTION] Part 2

let myString = 'supercalifragilisticexpialidocious';
console.log(myString);
let myConsonants = '';

for (let i = 0; i < myString.length; i++) {
    if (
        myString[i] == 'a' ||
        myString[i] == 'e' ||
        myString[i] == 'i' ||
        myString[i] == 'o' ||
        myString[i] == 'u'
    ) {
        continue;
    } else {
        myConsonants += myString[i];
    };
};

console.log(myConsonants);